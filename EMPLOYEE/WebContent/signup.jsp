<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>A Sign up page for the new Employee</title>
</head>
<body>
	<form  action="EmployeeAttendance?action=ep" method="POST">	
	
		<fieldset>
		<table  bgcolor="#f0f0f0">
		<%HttpSession hs=request.getSession(); %>
				<tr align="center">
					<td style="color:green"><h3>It seems like you are a New <b>USER</b> please  <u><b>SIGNUP!!!</b></u></h3><br/></td>
				</tr>			
				<tr >
					<td>Full Name:<input type="text" align="right" name="empname" value="<%= hs.getAttribute("USERNAME") %>" size="30"></td>
				</tr>
				<tr>
				    <td>Email id:<input type="text" name="mailid" align="right" size="30"></td>
				</tr>
				<tr>
					<td>Gender:<input type="radio" name="sex">Male<input type="radio" name="sex">Female</td>
				</tr>
				<tr>
					<td>AGE:<input type="text" name="empage" id="epn" align="right" size="30"></td>
				</tr>
				<tr>
					<td>DOJ:<input type="text" name="doj" size="30"></td>
				</tr>
		
				<tr>
					<td>Previous Employer Details:<br/><textarea name="empdetails" rows="10" cols="60"></textarea></td>
				</tr>
		</table>
		</fieldset>
		
		<fieldset>
		<table bgcolor="#f0f0f0">		
				<tr>
					<td>Father Name:<input type="text" name="faname" id="ebg" size="30"></td>
				</tr>
				<tr>
					<td>Current Address:<br/><textarea name="empca" id="eca" rows="5" cols="60"></textarea></td>
				</tr>
				<tr>
					<td>Permanent Address:<br/><textarea name="emppa" id="epa" rows="5" cols="60"></textarea></td>
				</tr>
				<tr>
					<td>Mobile No1:<input type="text" name="mono1"></td>
				</tr>
				<tr>
					<td>Mobile No2:<input type="text" name="mono2"></td>
				</tr>
		</table>
		</fieldset>
		
		<fieldset>
		<table  bgcolor="#f0f0f0">
				<tr><td><p>Emergency Contact Details</p></td></tr>
				<tr>
					<td>Emergency Contact Person:<input type="text" name="emperson" id="emp" size="30"></td>
				</tr>
				<tr>
					<td>Emergency Contact Person Relation:<input type="text" name="emprel" id="empr" size="30"></td>
				</tr>
				<tr>
					<td>Emergency Contact Person Address:<br/><textarea name="ecpa" rows="6" cols="60"></textarea></td>
				</tr>
				<tr>
					<td>Emergency Contact Person Number:<input type="text" name="empersonno" id="empno" size="30"></td>
				</tr>
			</table>
		</fieldset>
		
		<fieldset>
		<table  bgcolor="#f0f0f0">
				<tr><td><p>Hobbies:</p></td></tr>
				<tr><td><textarea name="hobbies" rows="5" cols="60"></textarea></td></tr>
		</table>
		</fieldset>
					
		<fieldset>
			<table  bgcolor="#f0f0f0">
				<%HttpSession ha=request.getSession(); %>
				<tr><td><p>Password:<input type="password" value="<%= ha.getAttribute("PASSWORD") %>" name="nepass" size="30"></p></td></tr>
				<tr> <td>	<input type="submit" name="sub" value="SUB"></td></tr>
			</table>
		</fieldset>
		</form>
</body>
</html>