<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>A login page for the Employee:</title>
</head>
<body>
	<fieldset>
	<form action="EmployeeAttendance?action=ep" method="post">
	<table>
		<tr >
			<td colspan="2" >
				<h2 align="center" style="color:blue">Press <u><b>ENTRY</b></u> if you are Entering the Office else <u><b>EXIT</b></u> if you are leaving the Office.</h2>
			</td>
		</tr>
		<tr>
			<td>
				<input type="submit" name="entry" value="ENTRY">
				<input type="submit" name="exit" value="EXIT">
			</td>
		</tr>
	</table>
	</form>
	</fieldset>
</body>
</html>