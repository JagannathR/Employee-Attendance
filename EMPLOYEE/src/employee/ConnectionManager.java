package employee;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionManager {
	private static Connection con=null;
	public static  Connection getConnection()
	{
		try
		{
			String URL="jdbc:oracle:thin:@localhost:1521:XE";
			String username="system";
			String password="oracle";
		
			Class.forName("oracle.jdbc.driver.OracleDriver");
			con=DriverManager.getConnection(URL, username, password);
		}
		catch(ClassNotFoundException e)
		{
			System.out.println("Connection not established");
		}
		catch(Exception f)
		{
			f.printStackTrace();
		}
		
		return con;
	}
}
