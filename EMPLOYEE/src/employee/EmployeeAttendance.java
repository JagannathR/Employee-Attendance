package employee;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.itextpdf.text.Document;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


public class EmployeeAttendance extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
	
		Connection con=null;
		
		String action=request.getParameter("action");
		
		String name1=request.getParameter("login");
		
		String value1=request.getParameter("entry");
		String value2=request.getParameter("exit");
		
		String act=request.getParameter("subhr");
		String unhr=request.getParameter("hrun");
		String pdhr=request.getParameter("hrpd");
		
		String un=request.getParameter("username");
		String pd=request.getParameter("userpass");
		
		String actu=request.getParameter("sub");
		
		if("ep".equals(action))
		{
			try
			{		
				if(name1!=null && "login".equalsIgnoreCase(name1) )
				{
					
					Statement stmt=null;
					ResultSet res=null;
					Class.forName("oracle.jdbc.driver.OracleDriver");
					con=ConnectionManager.getConnection();
					String s="select full_name from EMPLOYEE where FULL_NAME='"+un+"' and PASSWORD='"+pd+"'";
					stmt=con.createStatement();
					res=stmt.executeQuery(s);
					
					
					if(res.next()==true)
					{
						try
						{	
							RequestDispatcher rd=request.getRequestDispatcher("login.jsp");
							rd.forward(request, response);
						
					 	}
						catch(Exception e)
						{
							out.println("error in validation");
						}
					}
					else
					{
						
						String nu=request.getParameter("username");
						String np=request.getParameter("userpass");
						
						HttpSession hs=request.getSession(true);
						
						hs.setAttribute("USERNAME", nu);
						hs.setAttribute("PASSWORD", np);
						
						Statement stmt4=null;
						ResultSet res4=null;
						Class.forName("oracle.jdbc.driver.OracleDriver");
						con=ConnectionManager.getConnection();
						String s1="select count(*) from EMPLOYEE  where FULL_NAME='"+nu+"' and PASSWORD='"+np+"'";
						stmt4=con.createStatement();
						res4=stmt4.executeQuery(s1);
						
						if(res4.next())
						{
							try
							{
								response.sendRedirect("signup.jsp");
							}
							catch(Exception e)
							{
								e.printStackTrace();
							}
						}
						else
						{
							HttpSession hts=request.getSession();
							String msg="Invalid USER please try AGAIN!!";
							hts.setAttribute("ERROR", msg);
							try
							{
								response.sendRedirect("Entry.jsp");
							}
							catch(Exception e)
							{
								out.println("Error");
							}
						}
						
					}
				}
				
				else if("entry".equalsIgnoreCase(value1))
				{
						try
						{
							Statement stmt1=null;
							ResultSet res1=null;
							String now=new Date().toString();
							//String now1=now.toString();
							Class.forName("oracle.jdbc.driver.OracleDriver");
							con=ConnectionManager.getConnection();
							String s1="update EMPLOYEE set ENTRY='"+now+"' where FULL_NAME='dev' and PASSWORD='sys'";
							stmt1=con.createStatement();
							res1=stmt1.executeQuery(s1);
							
							System.out.println(res1);	
							RequestDispatcher rds=request.getRequestDispatcher("welcome.jsp");
							rds.forward(request, response);
								
						}
						catch(Exception e)
						{
							System.out.println("1");
						}
				}
				
				else if("exit".equalsIgnoreCase(value2))
				{
					try
					{
								
						Statement stmt2=null;
						ResultSet res2=null;
						String now=new Date().toString();
						//String name=now.toString();
						Class.forName("oracle.jdbc.driver.OracleDriver");
						con=ConnectionManager.getConnection();
						String s1="update EMPLOYEE set EXIT='"+now+"' where FULL_NAME='dev' and PASSWORD='sys'";
						stmt2=con.createStatement();
						res2=stmt2.executeQuery(s1);
							
						System.out.println(res2);
						RequestDispatcher dr=request.getRequestDispatcher("welcome.jsp");
						dr.forward(request, response);
								
					}
					catch(Exception e)
					{
						out.println("2error");
					}
							
				}
				
				else if("submitt".equalsIgnoreCase(act))
				{
					try
					{
						String hrun=request.getParameter("hrun");
						String hrup=request.getParameter("hrpd");
						
						Class.forName ("oracle.jdbc.driver.OracleDriver");
						con=ConnectionManager.getConnection();
						Statement st4=con.createStatement();
						ResultSet res4=st4.executeQuery("select full_name from EMPLOYEE where full_name='"+hrun+"' and password='"+hrup+"' ");
	            
						if(res4.next()==true)
						{
							if(hrun.equals(unhr) && hrup.equals(pdhr))
							{
								try
								{
									Class.forName ("oracle.jdbc.driver.OracleDriver");
									con=ConnectionManager.getConnection();
									Statement st3=con.createStatement();
									ResultSet res3=st3.executeQuery("select full_name from EMPLOYEE");
		            
									if(res3.next()==true)
									{
										try
										{
											/* Create Connection objects */
											Class.forName ("oracle.jdbc.driver.OracleDriver");
											Connection conn = ConnectionManager.getConnection();
											Statement stmt = conn.createStatement();
											/* Define the SQL query */
											ResultSet query_set = stmt.executeQuery("SELECT * FROM EMPLOYEE where full_name='"+hrun+"' and password='"+hrup+"' ");
											/* Step-2: Initialize PDF documents - logical objects */
											Document my_pdf_report = new Document();
											PdfWriter.getInstance(my_pdf_report, new FileOutputStream("F:/PROJECTS/EMPLOYEE/WebContent/pdf_report_employee_new_dev_sys.pdf"));
											my_pdf_report.open();            
									//		we have four columns in our table
											PdfPTable my_report_table = new PdfPTable(15);
									//		create a cell object
											PdfPCell table_cell;
											
											while (query_set.next())
											{                
												String F_NAME= query_set.getString("FULL_NAME");
												table_cell=new PdfPCell(new Phrase(F_NAME));
												my_report_table.addCell(table_cell);
												
												String EMAIL_ID=query_set.getString("EMAIL_ID");
												table_cell=new PdfPCell(new Phrase(EMAIL_ID));
												my_report_table.addCell(table_cell);
		                            
												String GENDER=query_set.getString("GENDER");
												table_cell=new PdfPCell(new Phrase(GENDER));
												my_report_table.addCell(table_cell);
												
												String AGE=query_set.getString("AGE");
												table_cell=new PdfPCell(new Phrase(AGE));
												my_report_table.addCell(table_cell);
												
												String DOJ=query_set.getString("DOJ");
												table_cell=new PdfPCell(new Phrase(DOJ));
												my_report_table.addCell(table_cell);
		                        
												String PREVIOUS_DETAILS=query_set.getString("PREVIOUS_DETAILS");
												table_cell=new PdfPCell(new Phrase(PREVIOUS_DETAILS));
												my_report_table.addCell(table_cell);
		                        
												String FATHER_NAME=query_set.getString("FATHER_NAME");
												table_cell=new PdfPCell(new Phrase(FATHER_NAME));
												my_report_table.addCell(table_cell);
		                        
												String CURR_ADD=query_set.getString("CURR_ADD");
												table_cell=new PdfPCell(new Phrase(CURR_ADD));
												my_report_table.addCell(table_cell);
												
												String PERMANENT_ADD=query_set.getString("PERMANENT_ADD");
												table_cell=new PdfPCell(new Phrase(PERMANENT_ADD));
												my_report_table.addCell(table_cell);
		                        
												String MOBILE_NO1=query_set.getString("MOBILE_NO1");
												table_cell=new PdfPCell(new Phrase(MOBILE_NO1));
												my_report_table.addCell(table_cell);
												
												String MOBILE_NO2=query_set.getString("MOBILE_NO1");
												table_cell=new PdfPCell(new Phrase(MOBILE_NO2));
												my_report_table.addCell(table_cell);
		                        
												String ECPN=query_set.getString("ECPN");
												table_cell=new PdfPCell(new Phrase(ECPN));
												my_report_table.addCell(table_cell);
		                        
//												String ECPR=query_set.getString("ECPR");
//												table_cell=new PdfPCell(new Phrase(ECPR));
//												my_report_table.addCell(table_cell);
//                    
//												String ECPA=query_set.getString("ECPA");
//												table_cell=new PdfPCell(new Phrase(ECPA));
//												my_report_table.addCell(table_cell);
//		                        
//												String ECP_NO=query_set.getString("ECP_NO");
//												table_cell=new PdfPCell(new Phrase(ECP_NO));
//												my_report_table.addCell(table_cell);
//		                        		
//												String HOBBIES=query_set.getString("HOBBIES");
//												table_cell=new PdfPCell(new Phrase(HOBBIES));
//												my_report_table.addCell(table_cell);
		                        	
												String PASSWORD=query_set.getString("PASSWORD");
												table_cell=new PdfPCell(new Phrase(PASSWORD));
												my_report_table.addCell(table_cell);
		                        	
												String ENTRY=(String)query_set.getString("ENTRY");
												table_cell=new PdfPCell(new Phrase(ENTRY));
												my_report_table.addCell(table_cell);
		                        	
												String EXIT=(String)query_set.getString("EXIT");
												table_cell=new PdfPCell(new Phrase(EXIT));
												my_report_table.addCell(table_cell);
		                        
											}
											/* Attach report table to PDF */
											my_pdf_report.add(my_report_table);                       
											my_pdf_report.close();
		            
											/* Close all DB related objects */
											query_set.close();
											stmt.close(); 
											conn.close();             
										}		
										
										catch(Exception e)
										{
											System.out.println("3");
									
										}
									}
									
									if(hrun.equals(unhr) && hrup.equals(pdhr))
									{
										response.sendRedirect("pdf_report_employee_new_dev_sys.pdf");
									}
								}
								catch(Exception e)
								{
									System.out.println("4");
									
								}		
							}
						}
					}
					catch(Exception e)
					{
						
					}
				}
				
//				else if("signup".equalsIgnoreCase(name2))
//				{
//					
//					String nu=request.getParameter("username");
//					String np=request.getParameter("userpass");
//					
//					HttpSession hs=request.getSession(true);
//					
//					hs.setAttribute("USERNAME", nu);
//					hs.setAttribute("PASSWORD", np);
//					
//					Statement stmt4=null;
//					ResultSet res4=null;
//					Class.forName("oracle.jdbc.driver.OracleDriver");
//					con=ConnectionManager.getConnection();
//					String s1="select count(*) from EMPLOYEE  where FULL_NAME='"+nu+"' and PASSWORD='"+np+"'";
//					stmt4=con.createStatement();
//					res4=stmt4.executeQuery(s1);
//					
//					if(res4.next())
//					{
//						try
//						{
//							response.sendRedirect("signup.jsp");
//						}
//						catch(Exception e)
//						{
//							e.printStackTrace();
//						}
//					}
//					else
//					{
//						HttpSession hts=request.getSession();
//						String msg="Invalid USER please try AGAIN!!";
//						hts.setAttribute("ERROR", msg);
//						try
//						{
//							response.sendRedirect("Entry.jsp");
//						}
//						catch(Exception e)
//						{
//							out.println("Error");
//						}
//					}
//				}
				
				else if("sub".equalsIgnoreCase(actu))
				{
					try
					{	
						String en=request.getParameter("empname");
						String mt=request.getParameter("mailid");
						String g=request.getParameter("sex");
						String ea=request.getParameter("empage");
						String doj=request.getParameter("doj");
						String ed=request.getParameter("empdetails");
						String fn=request.getParameter("faname");
						String ca=request.getParameter("empca");
						String pa=request.getParameter("emppa");
						String m1=request.getParameter("mono1");
						String m2=request.getParameter("mono2");
						String ecp=request.getParameter("emperson");
						String epr=request.getParameter("emprel");
						String epa=request.getParameter("ecpa");
						String epn=request.getParameter("empersonno");
						String ho=request.getParameter("hobbies");
						String ep=request.getParameter("nepass");
						
						try
						{
							Statement stmt4=null;
							ResultSet res4=null;
							Class.forName("oracle.jdbc.driver.OracleDriver");
							con=ConnectionManager.getConnection();
							String s4="insert into EMPLOYEE values('"+en+"','"+mt+"','"+g+"','"+ea+"','"+doj+"','"+ed+"','"+fn+"','"+ca+"','"+pa+"','"+m1+"','"+m2+"','"+ecp+"','"+epr+"','"+epa+"','"+epn+"','"+ho+"','"+ep+"','null','null')";
							stmt4=con.createStatement();
							res4=stmt4.executeQuery(s4);
							
							System.out.println("hi");
							
							while(res4.next())
							{
							out.println(en+"\n"+mt+"\n"+g+"\n"+ea+"\n"+doj+"\n"+ed+"\n"+fn+"\n"+ca+"\n"+pa+"\n"+m1+"\n"+m2+"\n"+ecp+"\n"+epr+"\n"+epa+"\n"+epn+"\n"+ho);
							}
						}
						catch(Exception e)
						{
							out.println("5");
						}
						
						
					}
					catch(Exception e)
					{
						out.println("6");
					}
					RequestDispatcher dr=request.getRequestDispatcher("welcome.jsp");
					dr.forward(request, response);
				}
				
				else
				{
					HttpSession hs=request.getSession();
					String msg="Invalid USER please try AGAIN!!";
					hs.setAttribute("ERROR", msg);
					try
					{
						response.sendRedirect("Entry.jsp");
					}
					catch(Exception e)
					{
						out.println("Error");
					}
				}
			}
			catch(ClassNotFoundException cnfe)
			{
				System.out.println("1");
				cnfe.printStackTrace();
			}
			catch(SQLException s)
			{
				System.out.println("2");
				s.printStackTrace();
			}
			catch(Exception e)
			{
				System.out.println("3");
				e.printStackTrace();
			}
		}	
	}
	
}
